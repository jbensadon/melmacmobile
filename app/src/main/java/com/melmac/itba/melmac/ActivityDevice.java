package com.melmac.itba.melmac;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class ActivityDevice extends AppCompatActivity {
    final static String NOTIFICATION_PREFERENCES_KEY = "NOTIFICATION_PREFERENCES";
    final static String FAVORITE_DEVICES_KEY = "FAVORITE_DEVICES";

    SharedPreferences sharedPrefs;
    HashSet<String> favoriteDevicesSet;

    String id;
    String name;
    boolean favorite;
    static DeviceInfo myInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent in = getIntent();
        id = in.getStringExtra("itba.melmac.com.device_id");
        myInfo = (DeviceInfo) in.getSerializableExtra("itba.melmac.com.device_info");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.favorite_icon:
                SharedPreferences.Editor prefEditor = sharedPrefs.edit();
                if (favoriteDevicesSet == null)
                    favoriteDevicesSet = new HashSet<>();
                if(favorite) {
                    favoriteDevicesSet.remove(id);
                    prefEditor.putStringSet(FAVORITE_DEVICES_KEY, favoriteDevicesSet);
                    prefEditor.apply();
                    prefEditor.commit();

                    Toast.makeText(getApplicationContext(),
                            R.string.is_now_not_favorite,
                            Toast.LENGTH_SHORT).show();
                    favorite = false;
                    item.setIcon(R.drawable.icon_favorite_off);
                }
                else{
                    favoriteDevicesSet.add(id);
                    prefEditor.putStringSet(FAVORITE_DEVICES_KEY, favoriteDevicesSet);
                    prefEditor.apply();
                    prefEditor.commit();

                    Toast.makeText(getApplicationContext(),
                            R.string.is_now_favorite,
                            Toast.LENGTH_SHORT).show();
                    favorite = true;
                    item.setIcon(R.drawable.icon_favorite_on);
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_device, menu);

        //We check if the device is in the favorites list and select the correct icon for the action bar accordingly
        sharedPrefs = getSharedPreferences(NOTIFICATION_PREFERENCES_KEY, Context.MODE_PRIVATE);
        favoriteDevicesSet = (HashSet<String>) sharedPrefs.getStringSet(FAVORITE_DEVICES_KEY, null);
        if (favoriteDevicesSet == null)
            favoriteDevicesSet = new HashSet<>();
        favorite = favoriteDevicesSet.contains(id);

        MenuItem favoriteItem = menu.findItem(R.id.favorite_icon);
        favoriteItem.setIcon(favorite? R.drawable.icon_favorite_on : R.drawable.icon_favorite_off);

        return super.onCreateOptionsMenu(menu);
    }

    public void getAPIState(final ResponseCallback callback){
        final JsonObjectRequest jarrayReq = new JsonObjectRequest(Request.Method.PUT,
                API.baseUrl + "devices/" + myInfo.id + "/" + "getState", new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //agarrar a todos
                            JSONObject jo = response.getJSONObject("result");
                            Log.d("StateResponse", jo.toString()); //testing
                            myInfo.loadDeviceState(jo);
                            callback.onSuccess();
                        }catch(Exception e){
                            Log.d("StateResponseErrorLvl1", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("StateError.Response", error.toString());
                    }
                });
        API.mRequestQueue.add(jarrayReq);
    }

    public void uploadChange(String changeName, Object params){
        final JSONArray body_params = new JSONArray();
        if(params != null){
            body_params.put(params);
        }
        final String jsonParams = body_params.toString();
        Log.d("BodyResponseTest", body_params.toString());
        final StringRequest jarrayReq = new StringRequest(Request.Method.PUT,
                API.baseUrl + "devices/" + myInfo.id + "/" + changeName,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("BodyResponse", response); //testing
                        }catch (Exception e){
                            Log.d("BodyResponseErrorLvl2", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("BodyError.Response", error.toString());
                    }
                })

        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody(){
                try {
                    return jsonParams == null ? null : jsonParams.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonParams, "utf-8");
                    return null;
                }
            }
        }
                ;


        API.mRequestQueue.add(jarrayReq);
    }


    /*@Override
    protected void onStop () {
        super.onStop();
        if (API.mRequestQueue != null) {
            API.mRequestQueue.cancelAll();
        }
    }*/

    public void changeStatus(Boolean bool){
        if(bool){
            uploadChange("turnOn", null);
        }else{
            uploadChange("turnOff", null);
        }
    }

    public void changeLockStatus(Boolean bool){
        if(bool){
            uploadChange("lock", null);
        }else{
            uploadChange("unlock", null);
        }
    }

    public void changeTemperature(Integer i){
        uploadChange("setTemperature", i);
    }

}
