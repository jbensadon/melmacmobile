package com.melmac.itba.melmac;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import java.util.HashSet;

public class NotificationService extends Service {
    final static String NOTIFICATION_PREFERENCES_KEY = "NOTIFICATION_PREFERENCES";
    final static String FAVORITE_DEVICES_KEY = "FAVORITE_DEVICES";
    final static String REFRESH_INTERVAL_KEY = "REFRESH_INTERVAL";

    HashSet<String> favoriteDevicesSet;
    long refreshIntervalInSeconds;

    @Override
    public IBinder onBind(Intent intent) {

        //Este código obtiene el Set con los IDs de los dispositivos favoritos (aquellos de los cuales el usuario quiere recibir notificaciones)
        //Y los guarda en favoriteDevicesSet. (Si no hay dispositivos favoritos, crea un nuevo set)
        SharedPreferences preferences = getSharedPreferences(NOTIFICATION_PREFERENCES_KEY, Context.MODE_PRIVATE);
        favoriteDevicesSet = (HashSet<String>) preferences.getStringSet(FAVORITE_DEVICES_KEY, null);
        refreshIntervalInSeconds = preferences.getLong(REFRESH_INTERVAL_KEY, 0);

        if (favoriteDevicesSet == null)
            favoriteDevicesSet = new HashSet<>();

        return null;
    }
}
