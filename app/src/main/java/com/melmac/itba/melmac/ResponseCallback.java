package com.melmac.itba.melmac;

import org.json.JSONObject;

public interface ResponseCallback {

    void onSuccess();

    void onFailure();

}
