package com.melmac.itba.melmac;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;

import java.util.HashSet;

public class ActivitySettings extends AppCompatActivity {
    Spinner refresh_dropdown;
    SharedPreferences preferences;
    long seconds_interval;

    SharedPreferences sharedPrefs;
    final static String NOTIFICATION_PREFERENCES_KEY = "NOTIFICATION_PREFERENCES";
    final static String REFRESH_INTERVAL_KEY = "REFRESH_INTERVAL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        refresh_dropdown = (Spinner) findViewById(R.id.refresh_dropdown);
        preferences = getSharedPreferences("PREFS", 0);

        // Spinner
        String modes[] = {getString(R.string.mins_1), getString(R.string.mins_5), getString(R.string.mins_10)};
        ArrayAdapter<String> adapterMode = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modes);
        adapterMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        refresh_dropdown.setAdapter(adapterMode);
        refresh_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                switch(position)
                {
                    case 0:     seconds_interval = 60;
                                break;
                    case 1:     seconds_interval = 60*5;
                                break;
                    case 2:     seconds_interval = 60*10;
                                break;
                }
                saveSettings();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
    }

    @Override
    public void onResume(){
        loadSettings();
        super.onResume();
    }

    private void loadSettings(){
        sharedPrefs = getSharedPreferences(NOTIFICATION_PREFERENCES_KEY, Context.MODE_PRIVATE);
        seconds_interval = sharedPrefs.getLong(REFRESH_INTERVAL_KEY, 0);
        if (seconds_interval == 60)
            refresh_dropdown.setSelection(0);
        else if (seconds_interval == 60*5)
            refresh_dropdown.setSelection(1);
        else if (seconds_interval == 60*10)
            refresh_dropdown.setSelection(2);
        else
            Log.d("SETTINGS_ACTIVITY", "Invalid refresh interval found in settings! Value= " + seconds_interval);
    }

    private void saveSettings(){
        SharedPreferences.Editor prefEditor = sharedPrefs.edit();
        prefEditor.putLong(REFRESH_INTERVAL_KEY, seconds_interval);
        prefEditor.apply();
        prefEditor.commit();
    }
}
