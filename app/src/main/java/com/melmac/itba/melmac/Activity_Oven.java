package com.melmac.itba.melmac;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

public class Activity_Oven extends ActivityDevice implements ResponseCallback{
    public static final String typeId = "im77xxyulpegfmv8";

    // Data to save
    boolean status, loading;
    Integer temperature;
    String heat_source, grill_mode, convection_mode;

    // Things we need for reading
    SwitchCompat status_switch;
    TextView temperature_slider_value_label;
    SeekBar temperature_slider;
    Spinner heat_source_dropdown;
    Spinner grill_mode_dropdown;
    Spinner convection_mode_dropdown;

    // Aux
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oven_);


        Intent in = getIntent();
        name = myInfo.name;
        id = myInfo.id;
        setTitle(name);
        preferences = getSharedPreferences("PREFS", 0);

        //valores iniciales
        status = false;
        temperature = 90;

        // Status Switch
        status_switch = (SwitchCompat) findViewById(R.id.status_switch);

        // Temperature Slider
        temperature_slider_value_label = (TextView) findViewById(R.id.temperature_slider_value_view);
        temperature_slider = (SeekBar) findViewById(R.id.temperature_slider);

        //Spinners
        heat_source_dropdown = (Spinner) findViewById(R.id.heat_source_dropdown);
        grill_mode_dropdown = (Spinner) findViewById(R.id.grill_mode_dropdown);
        convection_mode_dropdown = (Spinner) findViewById(R.id.convection_mode_dropdown);

        //Spinner Sources
        String sources[] = {getString(R.string.source_traditional), getString(R.string.source_upper),
                getString(R.string.source_lower)};
        String grill_modes[] = {getString(R.string.off), getString(R.string.grill_economical),
                getString(R.string.grill_complete)};
        String convection_modes[] = {getString(R.string.off), getString(R.string.convection_economical),
                getString(R.string.convection_traditional)};

        status_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = !status;
                status_switch.setChecked(status);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("status_switch", status);
                editor.apply();
                // Tell API
                changeStatus(status);
            }
        });

        temperature_slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                temperature_slider_value_label.setText((progress + 90) + "°C");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                temperature = progressChangedValue + 90;
                // Tell API
                changeTemperature(new Integer(temperature));
            }
        });

        final ArrayAdapter<String> adapterHeatSource = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sources);
        adapterHeatSource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        heat_source_dropdown.setAdapter(adapterHeatSource);
        heat_source_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                heat_source = adapter.getItemAtPosition(position).toString();

                // Tell API
                changeHeat(heat_source);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stu
            }
        });




        ArrayAdapter<String> adapterGrillMode = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, grill_modes);

        adapterGrillMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        grill_mode_dropdown.setAdapter(adapterGrillMode);
        grill_mode_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                grill_mode = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeGrill(grill_mode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });



        ArrayAdapter<String> adapterConvection = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, convection_modes);
        adapterConvection.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        convection_mode_dropdown.setAdapter(adapterConvection);
        convection_mode_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                convection_mode = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeConvection(convection_mode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update(){
        readState();
    }

    private void readState(){
        getAPIState(this);
    }

    @SuppressWarnings("ALL")
    public void onSuccess(){
        //leer localmente (por si no hay internet)
        //Log.d("StateResponseTest", myInfo.state.toString());
        setTitle(myInfo.name);
        status = new String(myInfo.state.get("status")).equals("on");
        status_switch.setChecked(status);

        temperature = Integer.valueOf(myInfo.state.get("temperature"));
        temperature_slider.setProgress(temperature-90);
        temperature_slider_value_label.setText((temperature) + "°C");

        heat_source = myInfo.state.get("heat");
        //Log.d("StateResponseTest",  myInfo.state.get("heat"));
        //checkear las 2 opciones por si la web-app utiliza la "alternativa"
        switch(heat_source.toUpperCase()){
            case "TRADITIONAL":
            case "CONVENTIONAL":
                heat_source_dropdown.setSelection(0);
                break;
            case "UPPER":
            case "TOP":
                heat_source_dropdown.setSelection(1);
                break;
            case "LOWER":
            case "BOTTOM":
                heat_source_dropdown.setSelection(2);
                break;
        }

        grill_mode = myInfo.state.get("grill");
        switch (grill_mode.toUpperCase()){
            case "OFF":
                grill_mode_dropdown.setSelection(0);
                break;
            case "ECONOMICAL":
            case "ECO":
                grill_mode_dropdown.setSelection(1);
                break;
            case "COMPLETE":
            case "LARGE":
                grill_mode_dropdown.setSelection(2);
                break;
        }
        convection_mode = myInfo.state.get("convection");
        switch (convection_mode.toUpperCase()){
            case "OFF":
                convection_mode_dropdown.setSelection(0);
                break;
            case "ECONOMICAL":
            case "ECO":
                convection_mode_dropdown.setSelection(1);
                break;
            case "TRADITIONAL":
            case "NORMAL":
                convection_mode_dropdown.setSelection(2);
                break;
        }
    }

    @Override
    public void onFailure() {
        onSuccess();
        Toast.makeText(getApplicationContext(), R.string.load_error, Toast.LENGTH_SHORT).show();
    }

    public void changeHeat(String heat_source){
        uploadChange("setHeat", heat_source);
    }

    public void changeGrill(String grill_mode){
        uploadChange("setGrill", grill_mode);
    }

    public void changeConvection(String convection_mode){
        uploadChange("setConvection", convection_mode);
    }


}