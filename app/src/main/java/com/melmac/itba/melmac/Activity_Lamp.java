package com.melmac.itba.melmac;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import yuku.ambilwarna.AmbilWarnaDialog;

public class Activity_Lamp extends ActivityDevice implements ResponseCallback{

    public static final String typeId = "go46xmbqeomjrsjr";

    boolean status;
    int brightness;
    int color_value;

    SwitchCompat status_switch;
    TextView brightness_slider_value_label;
    SeekBar brightness_slider;
    Button change_color_button;
    TextView color_value_label;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lamp_);
        Intent in = getIntent();
        name = in.getStringExtra("itba.melmac.com.device_name");
        setTitle(name);
        preferences = getSharedPreferences("PREFS", 0);

        // Status Switch
        status = false;
        status_switch = (SwitchCompat) findViewById(R.id.status_switch);

        // Brightness Slider
        brightness = 24;
        brightness_slider_value_label = (TextView) findViewById(R.id.brightness_slider_value_view);
        brightness_slider = (SeekBar) findViewById(R.id.brightness_slider);

        // Color picker
        color_value = getColor(R.color.colorAccent);
        color_value_label = findViewById(R.id.color_value_view);
        color_value_label.setText("#" +Integer.toHexString(color_value).substring(2,8).toUpperCase());
        change_color_button = (Button) findViewById(R.id.color_button);
    }

    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update(){
        readState();
    }

    private void readState(){
        getAPIState(this);
    }

    private void stateUI(){
        status_switch.setChecked(status);
        status_switch.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                status = !status;
                status_switch.setChecked(status);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("status_switch", status);
                editor.apply();
                // Tell API
                changeStatus(status);
            }
        });

        brightness_slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                brightness_slider_value_label.setText((progress) +"%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                brightness = progressChangedValue;
                // Tell API
                changeBrightness(brightness);
            }
        });

        color_value_label.setText("#" +Integer.toHexString(color_value).substring(2,8).toUpperCase());
        change_color_button.setBackgroundColor(color_value);
        change_color_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openColorPicker();
            }
        });
    }

    public void onSuccess(){
        stateUI();
        //Log.d("StateResponseTest", myInfo.state.toString());
        setTitle(myInfo.name);
        if(new String(myInfo.state.get("status")).equals("on")){
            status = true;
        }else{
            status = false;
        }
        status_switch.setChecked(status);

        brightness = Integer.valueOf(myInfo.state.get("brightness"));
        brightness_slider.setProgress(brightness);
        brightness_slider_value_label.setText(brightness +"%");

        // Read Color
        String hex = myInfo.state.get("color");
        color_value = Integer.parseInt(hex.replace("#", ""), 16);
        change_color_button.setBackgroundColor(Color.parseColor("#" + hex));
        color_value_label.setText("#" +hex.toUpperCase());
    }

    @Override
    public void onFailure() {
        onSuccess();
        Toast.makeText(getApplicationContext(), R.string.load_error, Toast.LENGTH_SHORT).show();
    }

    public void openColorPicker(){
        AmbilWarnaDialog colorpicker = new AmbilWarnaDialog(this, color_value, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                change_color_button.setBackgroundColor(color);
                color_value = color;
                color_value_label.setText("#" +Integer.toHexString(color).substring(2,8).toUpperCase());
                // Tell API
                changeColor(Integer.toHexString(color).substring(2,8).toUpperCase());
            }
        });
        colorpicker.show();
    }

    public void changeColor(String color){
        uploadChange("setColor", color);
    }

    public void changeBrightness(Integer i){
        uploadChange("setBrightness", i);
    }


}