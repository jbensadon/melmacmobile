package com.melmac.itba.melmac;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_Fridge extends ActivityDevice implements ResponseCallback{

    public static final String typeId = "rnizejqr2di0okho";

    // Data to save
    boolean favorite;
    boolean status;
    int fridge_temperature;
    int freezer_temperature;
    String mode;

    // Things we need for reading
    TextView fridge_temperature_slider_value_label;
    TextView freezer_temperature_slider_value_label;
    SeekBar fridge_temperature_slider;
    SeekBar freezer_temperature_slider;
    Spinner mode_dropdown;

    // Aux
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fridge_);

        Intent in = getIntent();
        name = in.getStringExtra("itba.melmac.com.device_name");
        id = in.getStringExtra("itba.melmac.com.device_id");
        setTitle(name);
        preferences = getSharedPreferences("PREFS", 0);

        fridge_temperature = 6;
        fridge_temperature_slider_value_label = (TextView) findViewById(R.id.fridge_temperature_slider_value_view);
        fridge_temperature_slider = (SeekBar) findViewById(R.id.fridge_temperature_slider);
        freezer_temperature = -16;
        freezer_temperature_slider_value_label = (TextView) findViewById(R.id.freezer_temperature_slider_value_view);
        freezer_temperature_slider = (SeekBar) findViewById(R.id.freezer_temperature_slider);
        mode_dropdown = (Spinner) findViewById(R.id.mode_dropdown);
    }

    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update(){
        readState();
    }

    private void readState(){
        getAPIState(this);
    }

    private void stateUI(){
        // Fridge Temperature Slider
        fridge_temperature_slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                fridge_temperature_slider_value_label.setText((progress + 2) + "°C");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            public void onStopTrackingTouch(SeekBar seekBar) {
                fridge_temperature = progressChangedValue + 2;
                // Tell API
                changeTemperature(fridge_temperature);
            }
        });

        // Freezer Temperature Slider
        freezer_temperature_slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                freezer_temperature_slider_value_label.setText((progress - 20) + "°C");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            public void onStopTrackingTouch(SeekBar seekBar) {
                freezer_temperature = progressChangedValue - 20;
                // Tell API
                changeFreezerTemperature(freezer_temperature);
            }
        });

        // Mode Spinner
        String modes[] = {getString(R.string.normal), getString(R.string.party_mode), getString(R.string.vacation_mode)};
        ArrayAdapter<String> adapterMode = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modes);
        adapterMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mode_dropdown.setAdapter(adapterMode);
        mode_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                mode = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeMode(mode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onSuccess() {
        stateUI();
        Log.d("StateResponseTest", myInfo.state.toString());
        setTitle(myInfo.name);
        fridge_temperature = Integer.valueOf(myInfo.state.get("temperature"));
        freezer_temperature = Integer.valueOf(myInfo.state.get("freezerTemperature"));

        fridge_temperature_slider.setProgress(fridge_temperature-2);
        fridge_temperature_slider_value_label.setText(fridge_temperature +"°C");
        freezer_temperature_slider.setProgress(freezer_temperature+20);
        freezer_temperature_slider_value_label.setText(freezer_temperature +"°C");

        mode = myInfo.state.get("mode");
        switch(mode.toUpperCase()){
            case "NORMAL":
                mode_dropdown.setSelection(0);
                break;
            case "PARTY MODE":
                mode_dropdown.setSelection(1);
                break;
            case "VACATION MODE":
                mode_dropdown.setSelection(2);
                break;
        }
    }

    @Override
    public void onFailure() {
        onSuccess();
        Toast.makeText(getApplicationContext(), R.string.load_error, Toast.LENGTH_SHORT).show();
    }

    public void changeFreezerTemperature(Integer i){
        uploadChange("setFreezerTemperature", i);
    }

    public void changeMode(String mode){
        uploadChange("setMode", mode);
    }
}