package com.melmac.itba.melmac;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class ListOfDevices extends AppCompatActivity {

    ArrayList<String> DEVICE_NAMES;
    ArrayList<String> DEVICE_INFO;
    ArrayList<String> DEVICE_IDS;
    ArrayList<String> DEVICE_TYPES;
    Intent aux;
    int index;
    DeviceInfo devInfaux;

    static String typeOfDeviceId;
    static HashMap<String, ArrayList<DeviceInfo>> deviceTypeMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_devices);

        // Cargo mi lista con los datos
        DEVICE_NAMES = new ArrayList<String>();
        DEVICE_INFO = new ArrayList<String>();
        DEVICE_IDS = new ArrayList<String>();
        DEVICE_TYPES = new ArrayList<String>();

        // Extraigo el elemento lista de la activity
        ListView listView = (ListView) findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(DEVICE_TYPES.get(position)){
                    case Activity_Oven.typeId:
                        aux = new Intent(getApplicationContext(), Activity_Oven.class);
                        index = deviceTypeMap.get(Activity_Oven.typeId).indexOf(DEVICE_IDS.get(position));
                        break;
                    case Activity_Fridge.typeId:
                        aux = new Intent(getApplicationContext(), Activity_Fridge.class);
                        index = deviceTypeMap.get(Activity_Fridge.typeId).indexOf(DEVICE_IDS.get(position));
                        break;
                    case Activity_Lamp.typeId:
                        aux = new Intent(getApplicationContext(), Activity_Lamp.class);
                        index = deviceTypeMap.get(Activity_Lamp.typeId).indexOf(DEVICE_IDS.get(position));
                        break;
                    case Activity_AC.typeId:
                        aux = new Intent(getApplicationContext(), Activity_AC.class);
                        index = deviceTypeMap.get(Activity_AC.typeId).indexOf(DEVICE_IDS.get(position));
                        break;
                    case Activity_Door.typeId:
                        aux = new Intent(getApplicationContext(), Activity_Door.class);
                        index = deviceTypeMap.get(Activity_Door.typeId).indexOf(DEVICE_IDS.get(position));
                        break;
                    case Activity_Blinds.typeId:
                        aux = new Intent(getApplicationContext(), Activity_Blinds.class);
                        index = deviceTypeMap.get(Activity_Blinds.typeId).indexOf(DEVICE_IDS.get(position));
                        break;
                }
                aux.putExtra("itba.melmac.com.device_name", DEVICE_NAMES.get(position));
                aux.putExtra("itba.melmac.com.device_id", DEVICE_IDS.get(position));
                aux.putExtra("itba.melmac.com.device_meta", DEVICE_INFO.get(position));
                devInfaux = new DeviceInfo(DEVICE_NAMES.get(position), DEVICE_IDS.get(position),
                        DEVICE_TYPES.get(position), "");
                aux.putExtra("itba.melmac.com.device_info", devInfaux);
                startActivity(aux);
            }
        });
    }

    @Override
    public void onResume(){
        update();
        super.onResume();
        switch (typeOfDeviceId) {
            case "appliances":
                setTitle(R.string.caption_appliances);
                break;
            case "go46xmbqeomjrsjr":
                setTitle(R.string.caption_lamps);
                break;
            case "li6cbv5sdlatti0j":
                setTitle(R.string.caption_ac);
                break;
            case "lsf78ly0eqrjbz91":
                setTitle(R.string.caption_doors);
                break;
            case "eu0v2xgprrhhg41g":
                setTitle(R.string.caption_blinds);
                break;
            default:
                setTitle(R.string.unknown); //cambiar para poder ser traducido
                break;
        }
    }

    void update(){
        TextView message = (TextView) findViewById(R.id.no_devices_found);
        ArrayList<DeviceInfo> aux = null;
        Log.d("", typeOfDeviceId);
        if(typeOfDeviceId != null) {

            if(typeOfDeviceId.equals("appliances")){
                ArrayList<DeviceInfo> temp = new ArrayList<DeviceInfo>();
                if(deviceTypeMap.get(Activity_Oven.typeId) != null){
                    temp.addAll(deviceTypeMap.get(Activity_Oven.typeId));
                }
                if(deviceTypeMap.get(Activity_Fridge.typeId) != null){
                    temp.addAll(deviceTypeMap.get(Activity_Fridge.typeId));
                }
                aux = temp;
            }else{
                if(deviceTypeMap.get(typeOfDeviceId) != null){
                    aux = deviceTypeMap.get(typeOfDeviceId);
                }
            }
            if (aux == null){
                message.setVisibility(View.VISIBLE);
                return;
            }

            message.setVisibility(View.GONE);
            for (DeviceInfo device : aux) {
                DEVICE_NAMES.add(device.name);
                DEVICE_INFO.add(device.meta);
                DEVICE_IDS.add(device.id);
                DEVICE_TYPES.add(device.type);
            }
        }
        else{
            message.setVisibility(View.VISIBLE);
        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return DEVICE_NAMES.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.device_list_element, null);
            TextView textView_name = (TextView) convertView.findViewById(R.id.device_name);
            TextView textView_subtitle = (TextView) convertView.findViewById(R.id.device_subtitle);
            textView_name.setText(DEVICE_NAMES.get(position));
            switch(DEVICE_TYPES.get(position))
            {
                case Activity_Oven.typeId:
                    textView_subtitle.setText(R.string.device_name_oven);
                    break;
                case Activity_Fridge.typeId:
                    textView_subtitle.setText(R.string.device_name_fridge);
                    break;
                case Activity_Lamp.typeId:
                    textView_subtitle.setText(R.string.device_name_lamp);
                    break;
                case Activity_AC.typeId:
                    textView_subtitle.setText(R.string.device_name_ac);
                    break;
                case Activity_Door.typeId:
                    textView_subtitle.setText(R.string.device_name_door);
                    break;
                case Activity_Blinds.typeId:
                    textView_subtitle.setText(R.string.device_name_blinds);
                    break;
                default:
                    textView_subtitle.setText(R.string.unknown_device_type); //cambiar para poder ser traducido
                    break;
            }
            return convertView;
        }
    }

}
