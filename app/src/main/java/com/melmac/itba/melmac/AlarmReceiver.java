package com.melmac.itba.melmac;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;


public class AlarmReceiver extends BroadcastReceiver {

    int id = 0;
    final static String NOTIFICATION_PREFERENCES_KEY = "NOTIFICATION_PREFERENCES";
    final static String FAVORITE_DEVICES_KEY = "FAVORITE_DEVICES";

    String name;

    HashSet<String> favoriteDevicesSet;

    Context context;


    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        //Este código obtiene el Set con los IDs de los dispositivos favoritos (aquellos de los cuales el usuario quiere recibir notificaciones)
        //Y los guarda en favoriteDevicesSet. (Si no hay dispositivos favoritos, crea un nuevo set)
        SharedPreferences preferences = context.getSharedPreferences(NOTIFICATION_PREFERENCES_KEY, Context.MODE_PRIVATE);
        favoriteDevicesSet = (HashSet<String>) preferences.getStringSet(FAVORITE_DEVICES_KEY, null);

        if (favoriteDevicesSet == null)
            favoriteDevicesSet = new HashSet<>();
        Log.d("AlarmReceiver", favoriteDevicesSet.toString());

        for (String id: favoriteDevicesSet){
            checkForDeviceUpdates(id);
        }
    }

    private void getDeviceInfoAndSendNotification(String deviceId, String action){

        TextView t;
        String s;
        switch(action) {
            case "freezerTemperatureChanged" :
                s = context.getResources().getString(R.string.freezer_temperature_of);
                break;

            case "modeChanged" :
                s = context.getResources().getString(R.string.mode_of);
                break;

            case "temperatureChanged" :
                s = context.getResources().getString(R.string.temperature_of);
                break;

            case "colorChanged" :
                s = context.getResources().getString(R.string.brightness_and_color_of);
                break;

            case "lockChanged" :
                s = context.getResources().getString(R.string.lock_of);
                break;

            case "verticalSwingChanged" :
                s = context.getResources().getString(R.string.v_blade_angle_of);
                break;

            case "horizontalSwingChanged" :
                s = context.getResources().getString(R.string.h_blade_angle_of);
                break;

            case "fanSpeedChanged" :
                s = context.getResources().getString(R.string.fan_speed_of);
                break;

            case "statusChanged" :
                s = context.getResources().getString(R.string.status_of);
                break;

            case "heatChanged" :
                s = context.getResources().getString(R.string.heat_source_of);
                break;

            case "convectionChanged" :
                s = context.getResources().getString(R.string.convection_mode_of);
                break;

            case "grillChanged" :
                s = context.getResources().getString(R.string.grill_mode_of);
                break;

            default :
                s = context.getResources().getString(R.string.device);
                break;

        }
        sendNotification(s ,deviceId);
    }

    private void sendNotification(String text, String deviceId){
        NotificationHelper notificationHelper = new NotificationHelper(context);
        id++;

        // Create an Intent for the activity you want to start
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = TaskStackBuilder.create(context)
                .addNextIntent(intent)
                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder nb = notificationHelper
                .getChannelNotification("MELMAC", text, deviceId, name)
                .setContentIntent(pendingIntent);
        notificationHelper.getManager().notify(id, nb.build());
    }

    private void checkForDeviceUpdates(final String deviceId){
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, API.baseUrl + "devices/" + deviceId + "/events",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Le damos formato a los datos recibidos y los dividimos en cada evento
                        String[] array = response.split("data:");
                        String processedResponse = arrayToString(array);
                        String[] eventsAsString = processedResponse.split("id: [0-9]*");
                            for (String str: eventsAsString){
                                //Acá se procesan los eventos y se extrae la info.
                                String[] actions = str.split("\"event\": \"");
                                if (actions.length > 1){
                                    String action = actions[1].split("\"")[0];

                                    //Y acá se la enviamos a la función que la procesa y manda la notificación.
                                    getDeviceName(deviceId, action);
                                }

                            }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Device Events error", "There was an error/ deviceID " + deviceId + ". Error message: " + error.toString());
            }
        });
        API.mRequestQueue.add(stringRequest);
    }

    public void getDeviceName(final String deviceId, final String action){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, API.baseUrl + "devices/" + deviceId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Le damos formato a los datos recibidos y conseguimos el nombre
                        Log.d("taggie",response);
                        String[] array = response.split("\"name\": \"");
                        String processedResponse = array[1];
                        String[] array2 = processedResponse.split("\"");
                        name = array2[0];
                        getDeviceInfoAndSendNotification(deviceId, action);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Device Events error", "There was an error/ deviceID " + deviceId + ". Error message: " + error.toString());
            }
        });
        API.mRequestQueue.add(stringRequest);
    }

    public static String arrayToString(Object[] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        for (int i = 0; ; i++) {
            b.append(String.valueOf(a[i]));
            if (i == iMax)
                return b.toString();
        }
    }



}

