package com.melmac.itba.melmac;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Tab3Routines extends Fragment {
    LinearLayout loadingIndicator;
    ListView listViewRoutines;
    TextView textViewNoRoutinesFound;
    Button retryButton;
    LinearLayout connectionErrorScreen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.tab3routines, container, false);

        listViewRoutines = rootView.findViewById(R.id.list_view_routines);
        connectionErrorScreen = rootView.findViewById(R.id.connection_error_screen);
        retryButton = rootView.findViewById(R.id.retry_button);
        textViewNoRoutinesFound = rootView.findViewById(R.id.no_routines_found);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });

        loadingIndicator = rootView.findViewById(R.id.loading_indicator_routines);
        loadingIndicator.setVisibility(View.VISIBLE);
        listViewRoutines.setVisibility(View.GONE);
        fetchRoutineListFromAPI();
        return rootView;
    }

    @Override
    public void onResume(){
        loadingIndicator.setVisibility(View.VISIBLE);
        listViewRoutines.setVisibility(View.GONE);
        connectionErrorScreen.setVisibility(View.GONE);
        fetchRoutineListFromAPI();
        super.onResume();
    }


    public void fetchRoutineListFromAPI() {
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, API.baseUrl + "routines", new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response",  response.toString());
                        try {
                            updateRoutineList(response, listViewRoutines, textViewNoRoutinesFound);
//                          Log.d("ResponseSuccess", response.getJSONArray("rooms").getJSONObject(0).getString("name"));
                            loadingIndicator.setVisibility(View.GONE);
                            listViewRoutines.setVisibility(View.VISIBLE);
                        }catch(JSONException e){
                            Log.d("ResponseError", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        loadingIndicator.setVisibility(View.GONE);
                        connectionErrorScreen.setVisibility(View.VISIBLE);
                    }
                });
        API.mRequestQueue.add(jsonReq);
    }

    public void runRoutineOnAPI(String routineId, final View playButton, final View progressBar){
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.PUT, API.baseUrl + "routines/" + routineId + "/execute", new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("RunRoutineResponse",  response.toString());
                        playButton.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        playButton.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), R.string.routine_error,
                                Toast.LENGTH_SHORT).show();
                    }
                });
        API.mRequestQueue.add(jsonReq);
    }

    void updateRoutineList(JSONObject response, ListView list, TextView noElementsErrorView) throws JSONException {
        JSONArray routineArray = response.getJSONArray("routines");
        if (routineArray.length() != 0)
            list.setAdapter(new RoutineListViewAdapter(getAttributeListFromJSON(routineArray, "name"),
                                                        getAttributeListFromJSON(routineArray, "id")));
        else
            noElementsErrorView.setVisibility(View.VISIBLE);
    }

    ArrayList<String> getAttributeListFromJSON(JSONArray array, String attribute) throws JSONException{
        ArrayList<String> result = new ArrayList<>();
        int i;
        for (i = 0; i < array.length(); i++){
            result.add(array.getJSONObject(i).getString(attribute));
        }
        return result;
    }

    class RoutineListViewAdapter extends BaseAdapter {

        private ArrayList<String> routineNames, routineIds, routineSubtitles;

        public RoutineListViewAdapter(ArrayList routineNames, ArrayList routineIds){
            this.routineIds = routineIds;
            this.routineNames = routineNames;
            Log.d("RoutinesListViewAdapter", routineIds.toString() + "\n" + routineNames.toString());
        }
        @Override
        public int getCount() {
            return routineNames.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            RelativeLayout view;

            if (convertView == null) { //If we get a new view, we inflate it and set its content
                view = (RelativeLayout) getLayoutInflater().inflate(R.layout.routine_list_element, parent, false);
                setViewContents(position, view);

            } else { //If it's a recycled view, we just set its contents (it's already inflated)
                view = (RelativeLayout) convertView;
                setViewContents(position, view);
            }

            return view;
        }

        private void setViewContents(final int position, View view){
            TextView textViewName = (TextView) view.findViewById(R.id.routine_name);
            textViewName.setText(routineNames.get(position));

            // Al clickear el botón de play cambia ícono hasta que termine de ejecutarse la rutina
            final ProgressBar progressBar = view.findViewById(R.id.loading_circle);
            final ImageButton playButton = view.findViewById(R.id.play_button);
            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    playButton.setVisibility(View.GONE);
                    Toast.makeText(getContext(),  getContext().getString(R.string.running_routine) +" " +routineNames.get(position),
                            Toast.LENGTH_SHORT).show();
                    runRoutineOnAPI(routineIds.get(position), playButton, progressBar);
                }
            });
        }
    }
}