package com.melmac.itba.melmac;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class Tab1Rooms extends Fragment {

    TextView roomName;
    View loadingIndicator;
    private static ArrayList<RoomInfo> roomInfoList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab1rooms, container, false);
        roomName = (TextView) rootView.findViewById(R.id.roomName);
        loadingIndicator = rootView.findViewById(R.id.loading_indicator_rooms);
        loadingIndicator.setVisibility(View.VISIBLE);
        update();
        return rootView;
    }

    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update() {

        JsonObjectRequest jarrayReq = new JsonObjectRequest(Request.Method.GET, API.baseUrl + "rooms", new JSONObject(),
                new Response.Listener<JSONObject>() {

                    RoomInfo aux = new RoomInfo();


                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response",  response.toString());
                        try {
                            if (response.getJSONArray("rooms").length() != 0){
                                roomName.setText(response.getJSONArray("rooms").getJSONObject(0).getString("name"));
                                Log.d("ResponseSuccess", response.getJSONArray("rooms").getJSONObject(0).getString("name"));
                            }
                            loadingIndicator.setVisibility(View.GONE);
                        }catch(JSONException e){
                            Log.d("ResponseError", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                });
        API.mRequestQueue.add(jarrayReq);
    }

    private class RoomInfo {
        String name, id, meta;
    }

    public class MyAdapter extends RecyclerView.Adapter {

        private Context mContext;
        private List mFlowerList;

        MyAdapter(Context mContext, List mFlowerList) {
            this.mContext = mContext;
            this.mFlowerList = mFlowerList;
        }

        @Override
        public FlowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        }

        public void onBindViewHolder(FlowerViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 0;
        }
    }

    class FlowerViewHolder extends RecyclerView.ViewHolder {


        FlowerViewHolder(View itemView) {
            super(itemView);
        }
    }

}


