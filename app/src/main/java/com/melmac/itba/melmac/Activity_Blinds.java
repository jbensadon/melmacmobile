package com.melmac.itba.melmac;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Activity_Blinds extends ActivityDevice implements ResponseCallback{

    public static final String typeId = "eu0v2xgprrhhg41g";
    // Data to save
    boolean favorite;
    boolean status;
    boolean lock_status;

    // Things we need for reading
    SwitchCompat status_switch;
    SwitchCompat lock_status_switch;

    // Aux
    SharedPreferences preferences;
    String id;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blinds_);

        Intent in = getIntent();
        name = in.getStringExtra("itba.melmac.com.device_name");
        id = in.getStringExtra("itba.melmac.com.device_id");
        setTitle(name);
        preferences = getSharedPreferences("PREFS", 0);
        status_switch = (SwitchCompat) findViewById(R.id.status_switch);
    }

    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update(){
        readState();
    }

    private void readState(){
        getAPIState(this);
    }

    private void stateUI(){
        // Status Switch
        status_switch.setChecked(status);
        status_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = !status;
                status_switch.setChecked(status);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("status_switch", status);
                editor.apply();
                // Tell API
                changeStatus(status);
            }
        });
    }

    @Override
    public void onSuccess() {
        stateUI();
        Log.d("StateResponseTest", myInfo.state.toString());
        String aux = new String(myInfo.state.get("status"));
        setTitle(myInfo.name);
        if(aux.equals("closed") || aux.equals("closing")){
            status = false;
        }else{
            status = true;
        }
        status_switch.setChecked(status);
    }

    @Override
    public void onFailure() {
        onSuccess();
        Toast.makeText(getApplicationContext(), R.string.load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void changeStatus(Boolean bool){
        if(bool){
            uploadChange("up", null);
        }else{
            uploadChange("down", null);
        }
    }
}