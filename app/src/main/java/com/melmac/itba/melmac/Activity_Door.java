package com.melmac.itba.melmac;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Activity_Door extends ActivityDevice implements ResponseCallback{

    public static final String typeId = "lsf78ly0eqrjbz91";

    // Data to save
    boolean favorite;
    boolean status;
    boolean lock_status;

    // Things we need for reading
    SwitchCompat status_switch;
    SwitchCompat lock_status_switch;

    // Aux
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_door_);

        Intent in = getIntent();
        name = in.getStringExtra("itba.melmac.com.device_name");
        id = in.getStringExtra("itba.melmac.com.device_id");
        setTitle(name);
        preferences = getSharedPreferences("PREFS", 0);

        // Status Switch
        status = false;
        status_switch = (SwitchCompat) findViewById(R.id.status_switch);

        // Lock Status Switch
        lock_status = false;
        lock_status_switch = (SwitchCompat) findViewById(R.id.lock_status_switch);
    }


    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update(){
        readState();
    }

    private void readState(){
        getAPIState(this);
    }

    private void stateUI(){
        status_switch.setChecked(status);
        status_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = !status;
                status_switch.setChecked(status);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("status_switch", status);
                editor.apply();
                // Tell API
                changeStatus(status);
            }
        });

        lock_status_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lock_status = !lock_status;
                lock_status_switch.setChecked(lock_status);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("lock_status_switch", lock_status);
                editor.apply();
                // Tell API
                changeLockStatus(lock_status);
            }
        });
    }

    @Override
    public void onSuccess() {
        stateUI();
        Log.d("StateResponseTest", myInfo.state.toString());
        setTitle(myInfo.name);
        status = new String(myInfo.state.get("status")).equals("opened");
        status_switch.setChecked(status);

        lock_status = new String(myInfo.state.get("lock")).equals("locked");
        lock_status_switch.setChecked(lock_status);
    }

    @Override
    public void onFailure() {
        onSuccess();
        Toast.makeText(getApplicationContext(), R.string.load_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void changeStatus(Boolean bool){
        if(bool){
            uploadChange("open", null);
        }else{
            uploadChange("close", null);
        }
    }
}