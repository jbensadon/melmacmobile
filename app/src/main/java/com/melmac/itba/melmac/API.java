package com.melmac.itba.melmac;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.os.Build;
import android.support.annotation.RequiresApi;

public class API {
    public final static String baseUrl = "http://10.0.2.2:8080/api/";
    // When running on emulator: "http://10.0.2.2:8000/api"
    // When running on USB phone: "http://192.168.0.19:8000/api"
    public static NotificationManager nManager;
    public static RequestQueue mRequestQueue;
    public static MainActivity mainActivity;
    public static int channelId;
    public static Gson gson;

    public static void init(MainActivity mActivity) {
        mainActivity = mActivity;
        channelId = 1;
        mRequestQueue = Volley.newRequestQueue(mainActivity);
        nManager = (NotificationManager) mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        gson = new GsonBuilder().create();
    }
}