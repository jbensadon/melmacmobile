package com.melmac.itba.melmac;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Tab2Devices extends Fragment {

    HashMap<String, ArrayList<DeviceInfo>> deviceTypeMap;

    View loadingIndicator;
    View cards;
    Button retryButton;
    LinearLayout connectionErrorScreen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2devices, container, false);
        CardView appliances_card = rootView.findViewById(R.id.appliances_card);
        CardView ac_card = rootView.findViewById(R.id.air_conditioners_card);
        CardView blinds_card = rootView.findViewById(R.id.blinds_card);
        CardView doors_card = rootView.findViewById(R.id.doors_card);
        CardView lamps_card = rootView.findViewById(R.id.lamps_card);
        deviceTypeMap = new HashMap<String, ArrayList<DeviceInfo>>();
        appliances_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_to_device_list_screen = new Intent(getContext(), ListOfDevices.class);
                /*ArrayList<DeviceInfo> aux = new ArrayList<>();
                if(deviceTypeMap.get("im77xxyulpegfmv8") != null)
                    aux.addAll(deviceTypeMap.get("im77xxyulpegfmv8"));
                if(deviceTypeMap.get("rnizejqr2di0okho") != null)
                    aux.addAll(deviceTypeMap.get("rnizejqr2di0okho"));
                go_to_device_list_screen.putExtra("itba.melmac.com.device_list", aux);*/
                ListOfDevices.typeOfDeviceId = "appliances";
                startActivity(go_to_device_list_screen);
            }
        });
        ac_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_to_device_list_screen = new Intent(getContext(), ListOfDevices.class);
                //go_to_device_list_screen.putExtra("itba.melmac.com.device_list_id", "li6cbv5sdlatti0j");
                ListOfDevices.typeOfDeviceId = "li6cbv5sdlatti0j";
                startActivity(go_to_device_list_screen);
            }
        });
        blinds_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_to_device_list_screen = new Intent(getContext(), ListOfDevices.class);
                //go_to_device_list_screen.putExtra("itba.melmac.com.device_list_id", "eu0v2xgprrhhg41g");
                ListOfDevices.typeOfDeviceId = "eu0v2xgprrhhg41g";
                startActivity(go_to_device_list_screen);
            }
        });
        doors_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_to_device_list_screen = new Intent(getContext(), ListOfDevices.class);
                //go_to_device_list_screen.putExtra("itba.melmac.com.device_list_id", "lsf78ly0eqrjbz91");
                ListOfDevices.typeOfDeviceId = "lsf78ly0eqrjbz91";
                startActivity(go_to_device_list_screen);
            }
        });
        lamps_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_to_device_list_screen = new Intent(getContext(), ListOfDevices.class);
                //go_to_device_list_screen.putExtra("itba.melmac.com.device_list_id", "go46xmbqeomjrsjr");
                ListOfDevices.typeOfDeviceId = "go46xmbqeomjrsjr";
                startActivity(go_to_device_list_screen);
            }
        });

        loadingIndicator = rootView.findViewById(R.id.loading_indicator_devices);
        cards = rootView.findViewById(R.id.cards);
        connectionErrorScreen = rootView.findViewById(R.id.connection_error_screen);
        retryButton = rootView.findViewById(R.id.retry_button);
        loadingIndicator.setVisibility(View.VISIBLE);
        cards.setVisibility(View.GONE);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });
        return rootView;
    }

    @Override
    public void onResume(){
        connectionErrorScreen.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.VISIBLE);
        update();
        super.onResume();
    }

    public void update() {
        deviceTypeMap.clear();
        final JsonObjectRequest jarrayReq = new JsonObjectRequest(Request.Method.GET,
                                 API.baseUrl + "devices", new JSONObject(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //agarrar a todos
                            JSONArray jArray = response.getJSONArray("devices");
                            //Log.d("Response", response.toString());
                            try {
                                //filtrar por dispositivos
                                JSONObject obj;
                                for(int i = 0; i < jArray.length(); i++){
                                    obj = jArray.getJSONObject(i);
                                    switch (obj.getString("typeId")){
                                        case "go46xmbqeomjrsjr": //Lampara
                                            addToMap("go46xmbqeomjrsjr", obj);
                                            break;
                                        case "li6cbv5sdlatti0j": //AC
                                            addToMap("li6cbv5sdlatti0j", obj);
                                            break;
                                        case "im77xxyulpegfmv8": //HORNO
                                            addToMap("im77xxyulpegfmv8", obj);
                                            break;
                                        case "lsf78ly0eqrjbz91": //PUERTA
                                            addToMap("lsf78ly0eqrjbz91", obj);
                                            break;
                                        case "rnizejqr2di0okho": //HELADERA
                                            addToMap("rnizejqr2di0okho", obj);
                                            break;
                                        case "ofglvd9gqX8yfl3l": //TIMER
                                            addToMap("ofglvd9gqX8yfl3l", obj);
                                            break;
                                        case "eu0v2xgprrhhg41g": //BLIND
                                            addToMap("eu0v2xgprrhhg41g", obj);
                                            break;
                                        case "mxztsyjzsrq7iaqc": //ALARMA
                                            addToMap("mxztsyjzsrq7iaqc", obj);
                                            break;
                                    }
                                }
                                //Log.d("ResponseSuccess", response.getJSONArray("devices").getJSONObject(0).getString("name"));
                                loadingIndicator.setVisibility(View.GONE);
                                cards.setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                Log.d("ResponseErrorLvl2", e.toString());
                                loadingIndicator.setVisibility(View.GONE);
                                connectionErrorScreen.setVisibility(View.VISIBLE);
                            }
                        }catch(JSONException e){
                            Log.d("ResponseErrorLvl1", e.toString());
                            loadingIndicator.setVisibility(View.GONE);
                            connectionErrorScreen.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        loadingIndicator.setVisibility(View.GONE);
                        connectionErrorScreen.setVisibility(View.VISIBLE);
                    }
                });
        API.mRequestQueue.add(jarrayReq);
        ListOfDevices.deviceTypeMap = deviceTypeMap;
    }

    public void addToMap(String typeId, JSONObject obj){
        if(!deviceTypeMap.containsKey(typeId)){
            deviceTypeMap.put(typeId, new ArrayList<DeviceInfo>());
        }
        try {
            DeviceInfo aux = new DeviceInfo(obj.getString("name"), obj.getString("id"),
                    obj.getString("typeId"), obj.getString("meta"));
            deviceTypeMap.get(typeId).add(aux);
        }catch(JSONException e){
            Log.d("ErrorAddingToMap", e.toString());
        }catch (NullPointerException e){
            Log.d("ErrorAddingToMap2", e.toString());
        }

    }
}

