package com.melmac.itba.melmac;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DeviceInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    String name, id, type, meta;

    Map<String, String> state;

    public DeviceInfo(String name, String id, String type, String meta){
        this.name = name;
        this.id = id;
        this.type = type;
        this.meta = meta;
        this.state = new HashMap<>();
    }

    public void loadDeviceState(JSONObject stateObj){
        Iterator<String> it = stateObj.keys();
        while(it.hasNext()){
            try{
                String key = it.next();
                state.put(key, stateObj.getString(key));
            }catch(JSONException e){
                Log.d("StateResponseOnLoading", e.toString());
            }
        }
    }

    public boolean equals(Object o){
        if(o == null) return false;
        if(!o.getClass().equals(this.getClass())) return false;
        if(o == this) return true;
        DeviceInfo di = (DeviceInfo) o;
        return di.id.equals(this.id);
    }


}