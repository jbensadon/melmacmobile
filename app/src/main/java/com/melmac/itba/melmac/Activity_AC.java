package com.melmac.itba.melmac;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class Activity_AC extends ActivityDevice implements ResponseCallback{
    public static final String typeId = "li6cbv5sdlatti0j";

    // Data to save
    boolean favorite;
    boolean status;
    int temperature;
    String mode;
    String v_blade_angle;
    String h_blade_angle;
    String fan_speed;

    // Things we need for reading
    SwitchCompat status_switch;
    TextView temperature_slider_value_label;
    SeekBar temperature_slider;
    Spinner mode_dropdown;
    Spinner vertical_blade_dropdown;
    Spinner horizontal_blade_dropdown;
    Spinner fan_speed_dropdown;

    // Aux
    SharedPreferences preferences;
    String id;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ac);

        Intent in = getIntent();
        name = in.getStringExtra("itba.melmac.com.device_name");
        id = in.getStringExtra("itba.melmac.com.device_id");
        setTitle(name);
        preferences = getSharedPreferences("PREFS", 0);

        status = false;
        temperature = 24;

        status_switch = (SwitchCompat) findViewById(R.id.status_switch);
        temperature_slider_value_label = (TextView) findViewById(R.id.temperature_slider_value_view);
        temperature_slider = (SeekBar) findViewById(R.id.temperature_slider);
        fan_speed_dropdown = (Spinner) findViewById(R.id.fan_speed_dropdown);
        vertical_blade_dropdown = (Spinner) findViewById(R.id.vertical_blade_angle_dropdown);
        horizontal_blade_dropdown = (Spinner) findViewById(R.id.horizontal_blade_angle_dropdown);
        mode_dropdown = (Spinner) findViewById(R.id.mode_dropdown);
    }


    @Override
    public void onResume(){
        update();
        super.onResume();
    }

    public void update(){
        readState();
    }

    private void readState(){
        getAPIState(this);
    }

    private void stateUI(){
        // Status Switch
        status_switch.setChecked(status);
        status_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = !status;
                status_switch.setChecked(status);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("status_switch", status);
                editor.apply();
                // Tell API
                changeStatus(status);
            }
        });

        // Temperature Slider
        temperature_slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                temperature_slider_value_label.setText((progress + 18) + "°C");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                temperature = progressChangedValue + 18;
                // Tell API
                changeTemperature(temperature);
            }
        });

        // Mode Spinner
        String modes[] = {getString(R.string.fan), getString(R.string.cold), getString(R.string.heat)};
        ArrayAdapter<String> adapterMode = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modes);

        adapterMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mode_dropdown.setAdapter(adapterMode);
        mode_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                mode = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeMode(mode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // Vertical Blade Spinner
        String vert_angles[] = {getString(R.string.auto), "22", "45", "67", "90"};
        ArrayAdapter<String> adapterVBlades = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vert_angles);
        adapterVBlades.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vertical_blade_dropdown.setAdapter(adapterVBlades);
        vertical_blade_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                v_blade_angle = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeVerticalSwing(v_blade_angle);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // Horizontal Blade Spinner
        String horz_angles[] = {getString(R.string.auto), "-90", "-45", "0", "45", "90"};
        ArrayAdapter<String> adapterHBlades = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, horz_angles);
        adapterHBlades.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        horizontal_blade_dropdown.setAdapter(adapterHBlades);
        horizontal_blade_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                h_blade_angle = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeHorizontalSwing(h_blade_angle);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // Fan Speed Spinner
        String speeds[] = {getString(R.string.auto), "25", "50", "75", "100"};
        ArrayAdapter<String> adapterSpeed = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, speeds);
        adapterSpeed.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fan_speed_dropdown.setAdapter(adapterSpeed);
        fan_speed_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                fan_speed = adapter.getItemAtPosition(position).toString();
                // Tell API
                changeFanSpeed(fan_speed);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void onSuccess(){
        stateUI();
        Log.d("StateResponseTest", myInfo.state.toString());
        setTitle(myInfo.name);
        //Switch
        if(new String(myInfo.state.get("status")).equals("on")){
            status = true;
        }else{
            status = false;
        }
        status_switch.setChecked(status);

        temperature = Integer.valueOf(myInfo.state.get("temperature"));
        temperature_slider.setProgress(temperature-18);
        temperature_slider_value_label.setText((temperature) + "°C");

        mode = myInfo.state.get("mode");
        switch(mode.toUpperCase()){
            case "FAN":
                mode_dropdown.setSelection(0);
                break;
            case "COOL":
                mode_dropdown.setSelection(1);
                break;
            case "HEAT":
                mode_dropdown.setSelection(2);
                break;
        }

        v_blade_angle = myInfo.state.get("verticalSwing");
        switch(v_blade_angle){
            case "auto":
                vertical_blade_dropdown.setSelection(0);
                break;
            case "22":
                vertical_blade_dropdown.setSelection(1);
                break;
            case "45":
                vertical_blade_dropdown.setSelection(2);
                break;
            case "67":
                vertical_blade_dropdown.setSelection(3);
                break;
            case "90":
                vertical_blade_dropdown.setSelection(4);
                break;
        }

        h_blade_angle = myInfo.state.get("horizontalSwing");
        switch(h_blade_angle){
            case "auto":
                horizontal_blade_dropdown.setSelection(0);
                break;
            case "-90":
                horizontal_blade_dropdown.setSelection(1);
                break;
            case "-45":
                horizontal_blade_dropdown.setSelection(2);
                break;
            case "0":
                horizontal_blade_dropdown.setSelection(3);
                break;
            case "45":
                horizontal_blade_dropdown.setSelection(4);
                break;
            case "90":
                horizontal_blade_dropdown.setSelection(5);
                break;
        }

        fan_speed = myInfo.state.get("fanSpeed");
        switch(fan_speed){
            case "auto":
                fan_speed_dropdown.setSelection(0);
                break;
            case "25":
                fan_speed_dropdown.setSelection(1);
                break;
            case "50":
                fan_speed_dropdown.setSelection(2);
                break;
            case "75":
                fan_speed_dropdown.setSelection(3);
                break;
            case "100":
                fan_speed_dropdown.setSelection(4);
                break;
        }
    }

    @Override
    public void onFailure() {
        onSuccess();
        Toast.makeText(getApplicationContext(), R.string.load_error, Toast.LENGTH_SHORT).show();
    }

    public void changeMode(String mode){
        uploadChange("setMode", mode);
    }

    public void changeVerticalSwing(String n){
        uploadChange("setVerticalSwing", n);
    }

    public void changeHorizontalSwing(String n){
        uploadChange("setHorizontalSwing", n);
    }

    public void changeFanSpeed(String speed){
        uploadChange("setFanSpeed", speed);
    }
}